class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:new, :edit, :update, :destroy, :create]
  expose(:user)
  expose(:product)
  expose(:products)
  expose(:reviews)
  expose(:category)

  def index
  end
end