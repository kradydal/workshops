# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.delete_all

users_list = [
  {
    id: 1, 
    firstname: "Anna", 
    lastname: "Kowalska", 
    email: "anna@kowalska.pl", 
    password: "12345678", 
    admin: false
  },
  {
    id: 2,
    firstname: "Jan",
    lastname: "Nowak",
    email: "jan@nowak.pl",
    password: "12345678",
    admin: false
  },
  {
    id: 3,
    firstname: "Michael",
    lastname: "Jordan",
    email: "michael@jordan.nba",
    password: "23232323",
    admin: false
  },
  {
    id: 4, 
    firstname: "Justyna",
    lastname: "Kowalczyk",
    email: "justyna@kowalczyk.pl",
    password: "12345678",
    admin: false
  },
  {
    id: 5,
    firstname: "Michał",
    lastname: "Jurecki",
    email: "michal@jurecki.pl",
    password: "12345678",
    admin: false
  },
  {
    id: 6,
    firstname: "Robert",
    lastname: "Kubica",
    email: "robert@kubica.wrc",
    password: "12345678",
    admin: true
  }
]

users_list.each do |user|
  User.create(user)
end

Category.delete_all

categories_list = [
    {
      id: 1,
      name: "Shoes"
     }, 
]

categories_list.each do |category|
  Category.create(category)
end

Product.delete_all

products_list = [
  {
    id: 1,
    title: "Asics Gel Saga Trainers",
    description: "Asics have been developing sports shoes, trainers and apparel for over 60 years, introducing design philosophies that have revolutionised the world of sports.",
    price: 120.89,
    category_id: 1,
    user_id: 1
  },
  {
    id: 2,
    title: "Saucony Jazz Low Pro Yellow/Blue Trainers",
    description: "Huge with runners – they were early pioneers of the triathlon – these retro runners are the last word in colour-clashing, performance shoes. The Jazz Original and Grid 9000 are our faves.",
    price: 85.72,
    category_id: 1,
    user_id: 2
  },
  {
    id: 3,
    title: "New Balance 446 White & Red Suede Mix Trainers",
    description: "Boston based brand, New Balance began life in the 1900s as an arch support company.",
    price: 92.86,
    category_id: 1,
    user_id: 2
  },
  {
    id: 4,
    title: "Nike Internationalist Blue Trainers",
    description: "Super cool trainers and hi-tops, lead the bold collection of footwear. Fashion-led collections reference directional detailing, with a punchy, modern colour palette taking centre stage, while comfort and durability remain at the brand's core.",
    price: 95.72,
    category_id: 1,
    user_id: 6
  },
  {
    id: 5,
    title: "Lacoste Orane 6 Canvas Stripe Slip On Plimsolls",
    description: "Lacoste's authentic sportswear roots fuse fashion with functionality as crisp whites and colour-pop finishes give a contemporary twist on timeless styling.",
    price: 78.57,
    category_id: 1,
    user_id: 3
  }
]

products_list.each do |product|
  Product.create(product)
end

Review.delete_all

rewievs_list = [
  { id: 1, content: "Very Good", rating: 5, product_id: 1, user_id: 1 },
  { id: 2, content: "Good", rating: 4, product_id: 1, user_id: 2 },
  { id: 3, content: "Average", rating: 3, product_id: 1, user_id: 3 },
  { id: 4, content: "Bad", rating: 2, product_id: 1, user_id: 4 },
  { id: 5, content: "Very Bad", rating: 1, product_id: 1, user_id: 5 },
]

rewievs_list.each do |review|
  Review.create(review)
end